<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Messages;
use App\Models\Users;
use App\Models\Prize;
use Illuminate\Support\Facades\Redis;
use App\Models\Drawed;
class SwooleServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'swoole:server';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'swoole websocket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //创建server
        $server = new \Swoole\WebSocket\Server("0.0.0.0", 9502);

        //连接成功回调
        $server->on('open', function (\Swoole\WebSocket\Server $server, $request) {

        });

        //收到消息回调
        $server->on('message', function (\Swoole\WebSocket\Server $server, $frame) {
            $content = json_decode($frame->data,true);
            if($content['type'] == 'draw'){
                $drawdata = $this->draw();
                foreach($server->connections as $fd){
                    $server->push($fd,json_encode($drawdata));
                }
            }else{
                $this->InsertMessage($content);
                //推送给所有链接
                foreach ($server->connections as $fd){
                    $server->push($fd,$frame->data);
                }
            }
        });

        //关闭链接回调
        $server->on('close', function ($ser, $fd) {
            $this->info($fd . '断开链接');
        });
        $server->start();
    }
    public function draw()
    {
        $position = 0; //公布位置
        $position = redis::get('position');
        $drawUser =  Drawed::where('id','=',$position)->orderBy('id','desc')->first();
        if(!$drawUser){
            return ['type'=>'server','content'=>'系统还未生成抽奖,当前位置'.$position];
        }
        if(Redis::hexists('Drawed',$drawUser['id'])){
            return ['type'=>'server','content'=>'该奖励已公布过,当前位置'.$position];
        }
        Redis::hset('Drawed',$drawUser['id'],$drawUser['prizeid']);
        $prize = Prize::where(['id'=>$drawUser['prizeid']])->orderBy('id','asc')->first();
        $users = Users::where(['is_send'=>1,'is_draw'=>0])->get(['id','name','code']);
        $users = $users->toArray();
        $Userinfo = Users::where(['id'=>$drawUser['userid']])->first();
        array_push($users,$Userinfo->toArray());
        shuffle($users);
        $nextPrize = Prize::where('id','>',$prize['id'])->first();
        Redis::set('position',$drawUser['id']); //更新位置
        return ['users'=>$users,'prize'=>$prize,'drawuser'=>$drawUser,'userinfo'=>$Userinfo,'nextPrize'=>$nextPrize,'type'=>'draw'];
    }
    public function InsertMessage($data)
    {
        if(empty($data['userid'])){
            return true;
        }
        return Messages::insert(['userid'=>$data['userid'],'addtime'=>time(),'content'=>$data['content'],'type'=>$data['type']]);
    }
}
