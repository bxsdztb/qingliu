<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Prize as Prizes;
use App\Models\Users;
use App\Models\Drawed;
use Illuminate\Support\Facades\Redis;
use DB;
class Prize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:prize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'command prize';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Prizes $prize)
    {
        $thePrize = $prize->where(['status'=>0,'draw_user'=>0])->orderBy('id','asc')->first();
        if(!$thePrize){
            echo '没有奖品啦!'.PHP_EOL;
            die;
        }
        $this->draw($thePrize);
    }
    public function draw($prize)
    {
        $users = new Users();
        $datas = $users->where(['is_send'=>1,'is_draw'=>0])->get();
        $usersArr = $datas->toArray();
//        dd($datas->toArray());
        shuffle($usersArr); //将顺序打乱
        $rand = mt_rand(0,count($usersArr));
        //开启事务
        DB::beginTransaction();
        $prizeStatus = Prizes::where(['id'=>$prize['id']])->update(['status'=>1,'draw_user'=>$usersArr[$rand]['id']]); //修改奖品表中的状态 抽奖状态 & 中奖人id
        $drawInfoStatus = Drawed::insertGetId([
            'userid'        =>  $usersArr[$rand]['id'],//中奖者id
            'prizeid'       =>  $prize['id'],        //奖品id
            'addtime'       =>  time(),              //添加时间
            'prize_name'    =>  $prize['prize_name'],//奖品名称
            'prize_num'     =>  $prize['num'],      //奖品数量
            'prize_lev'     =>  $prize['lev']       //奖品等级
        ]);                                         //将得到的中奖用户存入中奖表
        Redis::set('position',$drawInfoStatus);  //redis 设置抽奖位置
        $userStatus = $users->where(['id'=>$usersArr[$rand]['id']])->update(['is_draw'=>1]);
        if($prizeStatus && $drawInfoStatus && $userStatus){
            DB::commit();
            echo $prize['prize_name'].'已投入完成'.PHP_EOL;
        }else{
            echo $prize['prize_name'].'投入失败'.PHP_EOL;
            DB::rollBack();
        }
    }
}
