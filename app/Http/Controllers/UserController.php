<?php
/**
 * Created by PhpStorm.
 * User: qixiaozhe
 * Date: 2020-01-26
 * Time: 16:46
 */
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\Redis;
class UserController extends Controller
{
    public function Login(Request $request,Users $user)
    {
        $ip = $request->server->get('REMOTE_ADDR');
        if(!$ip){
            return response('身份异常');
        }
        if($request->isMethod('post')){
            $isLogin = $user->where(['code'=>$request->post('code')])->first();
            if(!$isLogin){
                return response()->view('user.login',['message'=>'你提供的抽奖码未找到相关大佬哦','username'=>'']);
            }
            $user->where(['id'=>$isLogin['id']])->update(['ip'=>$ip,'status'=>1]); //修改数据库中的ip地址
            Redis::hset('users:ip',$ip,json_encode(['id'=>$isLogin['id'],'name'=>$isLogin['name'],'code'=>$isLogin['code']])); //将信息与ip存入redis 用来做免登录
            session()->put('name',$isLogin['name']);
            session()->put('userid',$isLogin['id']);
            session()->put('code',$isLogin['code']);
            return response()->redirectTo('/');
        }else{
            return response()->view('user.login',['message'=>'','username'=>'']);
        }
    }
    public function addUser(Request $request,Users $users)
    {
        if(!$request->isMethod('post')){
            return response()->view('user.adduser');
        }else{
            if($users->where(['name'=>$request->post('username')])->first()){
                return response()->redirectTo('adduser');
            }
            $data = [
                'name'  =>  $request->post('username'),
                'password'  =>  null,
                'ip'        =>  null,
                'addtime'   =>  null,
                'code'      =>  rand(1000,9999),
            ];
            $users->insert($data);
            return response()->redirectTo('adduser');
        }
    }
    public function users(Users $users)
    {
        $data = $users->get();
        $sends = $users->where(['is_send'=>1])->count();
        return response()->view('user.users',compact('data','sends'));
    }
    public function remove(Users $users,Request $request)
    {
        $user = $users->where(['id'=>$request->post('id')])->first();
        if($user->delete()){
            return response()->json(['status'=>1]);
        }else{
            return response()->json(['status'=>0]);
        }
    }
    public function save(Users $users,Request $request)
    {
        $status = 1;
        if($request->get('status') == 1){
            $status = 0;
        }
        return $users->where(['id'=>$request->get('id')])->update(['is_send'=>$status]);
    }

    public function user()
    {

    }
    public function draws(Users $users)
    {

    }
}