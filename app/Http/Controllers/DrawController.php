<?php
/**
 * Created by PhpStorm.
 * User: qixiaozhe
 * Date: 2020-01-26
 * Time: 18:27
 */
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Drawed;
use Illuminate\Support\Facades\Redis;
class DrawController extends Controller
{
    public function index(Request $request)
    {
        $ip = $request->server->get('REMOTE_ADDR');
        if(!$ip){
            return response('身份异常');
        }
        if(!session()->get('userid')){
            //session 不存在userid
            $data = json_decode(Redis::hget('users:ip',$ip),true);
            if($data){
                //该ip存在redis中 取出数据存入redis
                session()->put('userid',$data['id']);
                session()->put('name',$data['name']);
                session()->put('code',$data['code']);
            }
        }
        $signal = $request->get('drawsignal');
        return response()->view('draw.index',compact('signal'));
    }
    public function draws(Drawed $drawed)
    {
        return response()->json(['data'=>$drawed->where(['is_show'=>1])->orderBy('drawed.id','asc')->leftJoin('users','drawed.userid','=','users.id')->get()]);
    }
    public function save(Drawed $drawed,Request $request)
    {
        $id = $request->get('id');
        return response()->json(['data'=>$drawed->where(['prizeid'=>$id])->update(['is_show'=>1])]);
    }
}