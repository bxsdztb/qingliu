<?php
namespace App\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\Prize;
class IndexController extends Controller
{
    public function index(Request $request,Prize $prize)
    {
        if(!$request->isMethod('post')){
            if(!session()->get('adminid')){
                return response()->redirectTo('/admin/login');
            }
            return response()->view('admin.index');
        }else{
            $data = [
                'prize_name'    =>  $request->post('prizename'),
                'lev'           =>  $request->post('prizetype'),
                'provider'      =>  $request->post('prizepublish'),
                'num'           =>  $request->post('prizecount'),
                'status'        =>  0
            ];
            $prize->insert($data);
            return response()->redirectTo('admin/index');
        }
    }
}