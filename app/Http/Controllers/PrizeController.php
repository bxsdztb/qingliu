<?php
/**
 * Created by PhpStorm.
 * User: qixiaozhe
 * Date: 2020-02-04
 * Time: 15:26
 */
namespace App\Http\Controllers;
use App\Models\Prize;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\Prize as PrizeModel ;
class PrizeController extends Prize
{
    public function getprizes(Request $request,PrizeModel $prizes)
    {
        return response()->json(['data'=>$prizes->where([])->get(),'next'=>$prizes->where(['status'=>0,'draw_user'=>0])->orderBy('id','asc')->first()]);
    }
}