<?php
/**
 * Created by PhpStorm.
 * User: qixiaozhe
 * Date: 2020-02-02
 * Time: 00:21
 */
namespace App\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
class AdminController extends Controller
{
    public function login(Request $request)
    {
        if(!$request->isMethod('post')){
            return response()->view('admin.login');
        }else{
            $admin = Admin::where(['username'=>$request->post('username')])->first();
            if(!$admin || $request->post('password') != $admin['password']){
                return response()->view('admin.login');
            }else{
                session()->put('adminid',$admin['id']);
                return response()->redirectTo('/admin/index');
            }
        }
    }
}