<?php
/**
 * Created by PhpStorm.
 * User: qixiaozhe
 * Date: 2020-02-04
 * Time: 15:07
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Prize extends Model
{
    protected $table = 'prizes';
    public $timestamps = false;
}