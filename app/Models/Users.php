<?php
/**
 * Created by PhpStorm.
 * User: qixiaozhe
 * Date: 2020-01-26
 * Time: 17:31
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Users extends Model
{
    protected $table = 'users';
    public $timestamps = false;
}