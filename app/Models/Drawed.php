<?php
/**
 * Created by PhpStorm.
 * User: qixiaozhe
 * Date: 2020-01-26
 * Time: 17:31
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Drawed extends Model
{
    protected $table = 'drawed';
    public $timestamps = false;
}