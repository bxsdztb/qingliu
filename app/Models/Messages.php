<?php
/**
 * Created by PhpStorm.
 * User: qixiaozhe
 * Date: 2020-01-26
 * Time: 17:31
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Messages extends Model
{
    protected $table = 'messages';
    public $timestamps = false;
    protected $fillable = [
        'userid',
        'addtime',
        'content',
        'type'
    ];
}