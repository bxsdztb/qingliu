<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::prefix('/')->group(function () {
    Route::any('login','UserController@login');
    Route::get('/','DrawController@index');
    Route::get('getprizes','PrizeController@getprizes');
    Route::get('getdraweds','DrawController@draws');
    Route::any('adduser','UserController@addUser');
    Route::any('users','UserController@users');
    Route::post('removeUser','UserController@remove');
    Route::any('saveuser','UserController@save');
    Route::get('user/{id?}','UserController@user');
    Route::get('getdraws','UsersController@draws');
    Route::get('drawsave','DrawController@save');
});
Route::prefix('admin')->group(function () {
    Route::any('login','AdminController@login');
    Route::any('index','IndexController@index');
});
