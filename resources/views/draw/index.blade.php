<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>清流</title>
	<meta charset="utf-8">
	<meta name="robots" content="all" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="pragma" Content="no-cach" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
	<meta http-equiv="keywords" content="" />
	<meta name="author" content="" />
	<meta name="copyright" content="" />
	<meta name="description" content="" />
	<meta name="HandheldFriendly" content="true" />
    <link rel="stylesheet" href="/draw/css/style-lottery.css">
    <link rel="stylesheet" href="/draw/css/animate.css">
    <link rel="stylesheet" href="/draw/css/default.css">
	<style>
		.lottery-content .content-detail span
		{
			font-size: 20px;
			margin-left: 20px;
		}
		.lottery-list
		{
			width: 75%;
			text-align: center;
			margin-right: 17%;
		}
        .{{session()->get('name')}}-text
        {
            color:red;
            !important;
        }
        .wrap-main .lottery-wrap .lottery-list {
            width: 75%;
            text-align: center;
            margin-right: 17%;
            margin-top: 4%;
        }
	</style>
    <script>
        signal("{{$signal}}");
        function signal(draw_signal)
        {
            if(draw_signal != ''){
                var waitTime = 5;
                var waitIntVal = setInterval(function(){
                    waitTime = waitTime - 1;
                    if(waitTime == 0){
                        clearInterval(waitIntVal);
                        var data = {
                            'type': 'draw',
                            'content': 'loaddraw',
                            'userid': 'thisServer',
                            'username': 'thisServer'
                        };
                        send(data);
                    }
                },500);
            }
        }
    </script>
</head>
<body style="">
<!-- 顶部中国风花纹 start -->
<header class="top-head"></header>

<!--倒计时展示-->
<div class="stop-main" style="line-height: calc(100%);">
    <div id="stop-time">叁</div>
    <div class="back"></div>
</div>

<button class="close" id="modal-close" data-dismiss="modal">
    <span aria-hidden="true" class="btn_x">&times;</span>
</button>

<div class="modal fade" id="lottery-result" role="dialog" aria-hidden="true">
	<canvas id="canvas"></canvas>
    <div class="modal-dialog">
        <div class="modal-content" style="height: calc(100% - 110px);padding-top: 110px;" >
            <canvas id="lottery-canvas"></canvas>
        </div>
    </div>
</div>

<div class="main" style="height: calc(45% - 50px);width: 100%;position: absolute;">
    <div class="lotterty-infogo">
    </div>
    <div id="lottery-main" class="lottery-main" style="height: calc(100% - 60px);">
        <div class="wrap-border-main" style="height: calc(100% - 50px);">
            <img src="/draw/img/wrap-border-1.png" class="wrap-border wrap-border-1">
            <img src="/draw/img/wrap-border-2.png" class="wrap-border wrap-border-2">
            <img src="/draw/img/wrap-border-3.png" class="wrap-border wrap-border-3">
            <img src="/draw/img/wrap-border-4.png" class="wrap-border wrap-border-4">
            <div class="wrap-border wrap-border-left"></div>
            <div class="wrap-border wrap-border-right"></div>
        </div>
        <div class="wrap-main" style="height: calc(140% - 140px);">
            <div class="lottery-wrap" id="lottery-wrap"></div>
        </div>
    </div>
</div>
<div class="prize-list" style="cursor: pointer;display: none;">
    <div class="prize-list-body">
        <ul class="prize-list-dom">

        </ul>
    </div>
</div>
<div class="prize">

        <p class="prize-info">
            <span style="color:#ff0000;">当前还未开始抽奖</span>
        </p>
        <a href="Javascript:;" onclick="show('prize-list')" style="cursor: pointer;">查看奖品列表</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="Javascript:;" onclick="drawedlist('prize-list')" style="cursor: pointer;color:red;">查看中奖列表</a>
</div>

<div class="chat-main">
    <div class="chat-list"  style="cursor: pointer;">
    </div>
    <div class="icons"  status="0">
        <div class="icons-list" style="cursor: pointer;">
            @for($i = 1;$i <= 24 ;$i++)
                <span class="icon" key="{{$i}}" style="background: url(/css/icons/{{$i}}.svg) center no-repeat;cursor: pointer;"></span>
            @endfor
        </div>
    </div>
    <div class="chat-foot">
        <div class="chat-input">
            <form action="" onsubmit="return commit()">
                <input placeholder="我也讲两句..." type="text" class="messagebox">
                <input type="submit" style="display: none;">
            </form>
        </div>
        <div class="chat-input icon-send" style="cursor: pointer;">

        </div>
        <div class="chat-input icon-default" style="cursor: pointer;">

        </div>
        @if(!session()->get('userid'))
            <div class="chat-input icon-login" onclick="location.href='/index.php/login';" style="cursor: pointer;">

            </div>
        @else
            <div class="chat-input icon-me" onclick="" style="cursor: pointer;">

            </div>
        @endif
    </div>
</div>
<div class="me" style="display: none;cursor: pointer;">
    <span class="name">昵称:{{session()->get('name')}}</span>
    <span class="code">抽奖码:{{session()->get('code')}} :已参与</span>
</div>
<canvas class="snow-canvas" speed="2" interaction="true" size="10" count="30" start-color="rgba(253,252,251,1)" end-color="rgba(251,252,253,0.3)" opacity="0.00001" wind-power="2" image="false" width="1366" height="667"></canvas>
<canvas class="snow-canvas" speed="3" interaction="true" size="12" count="80" wind-power="-5" image="./img/snow.png" width="1366" height="667"></canvas>

<footer class="footer"></footer>

<!-- 参与抽奖人员list start -->
<script type="text/template" id="lotterycon-tpl">
    <div class="clearfix lottery-list draw-users  code-'+user.code+'" code="'+user.code+'" data-id="" data-namezh="">
                <div class="f-l lottery-content">
                        <em class="beauty border-01"></em>
                        <em class="beauty border-02"></em>
                        <em class="beauty border-03"></em>
                        <em class="beauty border-04"></em>
                        <div class="border bor-top"></div>
                        <div class="border bor-bottom"></div>
                        <div class="content-detail">
                <span>抽奖还未开始哦</span>
                           </div>
                    </div>
          </div>

</script>
<!-- 参与抽奖人员list stop -->
<script>
    //接收抽奖信号

</script>

<script type="text/javascript" src="/draw/js/underscore.js"></script>
<script src="http://www.jq22.com/jquery/jquery-1.10.2.js"></script>
<script type="text/javascript" src="/draw/lottery_data.js"></script>
<script src="/draw/js/boot-modal.js"></script>
<script src="/draw/js/snow-plugin.js"></script>
<script type="text/javascript" src="/draw/pc_draw.js" ></script>
</body>
</html>
<script src="/js/jquery.touch.js"></script>
<script>
    //获取中奖信息
    function drawedlist(dom)
    {
        $('.'+dom).show();
        $.ajax({
            url:'/index.php/getdraweds',
            data:{},
            type:'get',
            dataType:'json',
            success:function(response){
                var draweds =  '';
                if(response.data.length == ''){
                    $('.prize-list-dom').html('<li class="prize-type-sp" >暂无中奖者</li>');
                    return false;
                }
                $.each(response.data,function(key,draw){
                        draweds += '<li class="prize-type-sp" >'+draw.name+' - '+draw.prize_name+' x '+draw.prize_num+'</li>';
                });
                $('.prize-list-dom').html(draweds);
            }
        });
    }

    var Result = '';
    var prizeNextTotal = 0; //当前奖品后还剩几个未抽取的
    function drawcss(code)
    {
        var users = $('.draw-users');
        var spped = 20;
        var inteval = setInterval(function(){
            speedUp(spped);
            spped = spped-1;
            if(spped == 1){
                clearInterval(inteval);
                steping(code);
            }
        },500);
    };
    //获取参与抽奖的人员
    function getDrawUsers()
    {
        $.ajax({
            url:"/getdraws",
            data:{},
            dataType:'',
            type:'get',
            success:function(response){

            }
        });
    }
    function steping(draw_index) {
        isLock = false;
        var moveDom = document.getElementById('lottery-wrap');

        // 当isLock 锁还没解锁时， 此时不能停止抽奖，将会抛出没结束的异常
        if (isLock) {
            console.log('还没结束，请稍等...');
            return;
        }else{
            clearInterval(interval)
        }
        isStart = false;
        isMove = false;
        speed = 8;

        /*-------- 随机数停止方案 --------*/
        // 获取当前总的抽奖框
        var lottery_size = $('#lottery-wrap .lottery-list');
        var stop_index = '';
        var index_width = 110;
        var stop_top = 0;
        var draw_dom = '';
        for(var i = 0;i < lottery_size.size();i++){
            index_width = lottery_size[i].offsetWidth;
            if(lottery_size[i].getAttribute('code') == draw_index){
                // console.log($('.code-'+draw_index).offset().top);
                // console.log(lottery_size[i].offsetWidth);
                stop_index = lottery_size[i].offsetWidth * i;
                // console.log(lottery_size[i].style.width);
                stop_top = lottery_size[i].offsetWidth * i + 50;
                draw_dom = lottery_size[i].innerHTML;
                break;
            }else{
                lottery_size[i].remove(lottery_size[i].selectedIndex);
            }
        }
        // console.log(draw_dom);
        // $.each(lottery_size,function(k,v){
        //     index_width = v.offsetWidth;
        //    if(v.getAttribute('code') == draw_index){
        //         stop_index = k;
        //         stop_top = item_outer_height * stop_index;
        //         continue;
        //    }
        // });
        if(stop_index == ''){
            console.log('未得到停止位置.');
        }
        // 将整个抽奖块移动到停止索引所在位置 top 值
        // $('#lottery-wrap').css('top', -stop_top);
        // return false;
        // 停止动画时要走的距离，即再走三个索引（即两个框+半个框的距离）
        var left_distance = -index_width;
        var sure_index = stop_index;

        // 移动到要到达的指定位置
        var lastStep = function() {
            time02 = nextFrame(function() {
                moveDom.style.top = '-'+ 20 + '%';
                if (-top <= left_distance) {
                    lastStep();
                } else {
                    cancelFrame(time02);
                    // 处理中奖后的相关样式效果
                }
            });
        };
        lastStep();-
            // 停止动画
            cancelFrame(timer);

        var award = $('#lottery-btn').data('award');
        var lottery_name_zh = $('#lottery-wrap .lottery-list').eq(sure_index).data('namezh');
        var lottery_name_en = $('#lottery-wrap .lottery-list').eq(sure_index).data('nameen');

        // 移动完剩下的尺度
        var top = 0;
        var time02 = null;
        var waitTime = 2;
        var waitDrawInfoIntVal = setInterval(function(){
            waitTime = waitTime -1;
            if(waitTime == 0){
                clearInterval(waitDrawInfoIntVal);
                showDrawInfo();
            }
        },500);
    }
    function showDrawInfo()
    {
        var data = Result;
        alert('恭喜:'+data.userinfo.name+'喜提【'+data.prize.prize_name+' x  '+data.prize.num+'】');
        var prizetype = ''; //奖品分类
        switch (data.nextPrize.lev) {
            case 1:
                prizetype = 'sp';
                break;
            case 2:
                prizetype = 'ssr';
                break;
            case 3:
                prizetype = 'sr';
                break;
        }
        var prizeHtml = ' <p class="prize-info">\n' +
            '            <span style="color:#ff0000;">下一个奖品:</span>\n' +
            '            <span class="prize-type-'+prizetype+' prize-name" style="font-weight:800;">'+data.nextPrize.prize_name+'</span>\n' +
            '            <span style="color:#ff0000;" class="prize-count"> x '+data.nextPrize.num+'</span>\n' +
            '        </p>\n' +
            '        <a href="Javascript:;" onclick="show(\'prize-list\')" style="cursor: pointer;">查看奖品列表</a>&nbsp;&nbsp;&nbsp;&nbsp;' +
            '<a href="Javascript:;" onclick="drawedlist(\'prize-list\')" style="cursor: pointer;color:red;">查看中奖列表</a>';
        $('.prize').html(prizeHtml);
        $.ajax({
            url:'/index.php/drawsave',
            data:{id:data.prize.id},
            dataType:'json',
            type:'get',
            success:function(response){

            }
        });
    }
    $(document).on('click','.me',function(){
        $(this).hide();
    });
    $(document).on('click','.icon-me',function(){
        $('.me').show();
    });
    function show(Dom){
        //异步查询奖品
        $.ajax({
            url:'/index.php/getprizes',
            data:{},
            dataType:'json',
            type:'get',
            success:function(response){
                var prizelist = ''; //定义空字符串用来拼接dom
                $.each(response.data,function(key,prize){
                    var prizetype = ''; //奖品分类
                    switch (prize.lev) {
                        case 1:
                            prizetype = 'sp';
                            break;
                        case 2:
                            prizetype = 'ssr';
                            break;
                        case 3:
                            prizetype = 'sr';
                            break;
                    }
                    //拼接奖品dom
                    prizelist += '<li class="prize-type-'+prizetype+'" id="prize-'+prize.id+'">'+prize.prize_name+' * '+prize.num+'</li>';
                });
                $('.prize-list-dom').html(prizelist);
                $('#prize-'+response.next.id).append('<span class="this-prize"></span>'); //向下一个奖品添加标记
            }
        });
        $('.'+Dom).show();
    }
    $(document).on('click','.prize-list',function(){
        //点击奖品列表隐藏
        $(this).hide();
    });
    function commit()
    {
        var message = $('.messagebox').val();
        if(message == ''){
            //文本消息为空 直接返回false
            return false;
        }
        if("{{session()->get('userid')}}" == ''){
            //判断是否登录
            alert('大人需要登录才能发言哦!');
            return false;
        }
        $('.messagebox').val('');
        var data = {
            'type': 'text',
            'content': message,
            'userid': '{{session()->get('userid')}}',
            'username': '{{session()->get('name')}}'
        };
        send(data);
        return false;
    }
    $(document).on('click','.icon-send',function(){
        var message = $('.messagebox').val();
        if(message == ''){
            //文本消息为空 直接返回false
            return false;
        }
        if("{{session()->get('userid')}}" == ''){
            alert('大人需要登录才能发言哦!');
            return false;
        }
        $('.messagebox').val('');
        var data = {
            'type': 'text',
            'content': message,
            'userid': '{{session()->get('userid')}}',
            'username': '{{session()->get('name')}}'
        };
        send(data);
    });
    $(document).on('click','.icon',function(){
        if("{{session()->get('userid')}}" != '') {
            var data = {
                'type': 'icon',
                'content': $(this).attr('key'),
                'userid': '{{session()->get('userid')}}',
                'username': '{{session()->get('name')}}'
            };
            send(data);
        }else{
            alert('大佬您还没有登录奥!');
        }
        $('.icons').hide();
        $('.icons').attr('status',0);
    });
    $(document).on('click','.icon-default',function(){
        var status = $('.icons').attr('status');
        if(status == 1){
            $('.icons').hide();
            $('.icons').attr('status',0);
        }else{
            $('.icons').show();
            $('.icons').attr('status',1);
        }
    });
    if(window.WebSocket){
        // 端口和ip地址对应不要写错
        var webSocket = new WebSocket("ws://139.9.113.139:9502");
        webSocket.onopen = function (event) {
            if("{{session()->get('userid')}}" != ''){
                var logindata = JSON.stringify({'type':'login','content':'大佬驾到','userid':'{{session()->get('userid')}}','username':'{{session()->get('name')}}'});
                // console.log(logindata);
                webSocket.send(logindata);
            }
        };
        //收到服务端消息回调
        webSocket.onmessage = function (event) {
            var ChatLength = $('.chat-boxs').length;
            var data = JSON.parse(event.data);
            var color = '#f3eaea';
            if(data.username == '{{session()->get('name')}}'){
                color = 'red';
            }
            //判断返回的消息类型 text icon serverinfo
            switch (data.type) {
                case 'icon':
                    var NewChatBox = '<div class="chat-boxs chat-icon">\n' +
                        '            <span style="color:'+color+';">'+data.username+'</span>：<svg style="background: url(/css/icons/'+data.content+'.svg) center no-repeat;"></svg>\n' +
                        '        </div><br class="chat-br"> ';
                    break;
                case 'text':
                    var NewChatBox = '<div class="chat-boxs">\n' +
                        '            <span style="color:'+color+';" class="'+data.username+'-text">'+data.username+'</span><font>：'+data.content+'</font>\n' +
                        '        </div><br class="chat-br"> ';
                    break;
                case 'login':
                    var NewChatBox = '<div class="chat-boxs">\n' +
                        '            <span style="color:#50ec00;">'+data.username+''+data.content+'</span>\n' +
                        '        </div><br class="chat-br"> ';
                    break;
                case 'draw':
                    console.log(data);
                    setDrawInfo(data);
                    Result = data;
                    break;
                case 'server':
                    console.log(data);
                    break;
            }
            // $('.chat-boxs').first().remove();
            // $('.chat-br').first().remove();
            var showContent = $(".chat-list");
            $('.chat-list').append(NewChatBox);
            showContent[0].scrollTop = showContent[0].scrollHeight;
        }
    }else{
        console.log("您的浏览器不支持WebSocket");
    }
    function send(data)
    {
        //发送数据
        var datas = JSON.stringify(data);
        webSocket.send(datas);
    }
    function setDrawInfo(data)
    {
        var prizetype = '';
        switch (data.prize.lev) {
            case (1):
                prizetype = 'sp';
                break;
            case (2):
                prizetype = 'ssr';
                break;
            case (3):
                prizetype = 'sr';
                break;
        }

        //奖品信息
        var prizeHtml = ' <p class="prize-info">\n' +
            '            <span style="color:#ff0000;">当前奖品:</span>\n' +
            '            <span class="prize-type-'+prizetype+' prize-name" style="font-weight:800;">'+data.prize.prize_name+'</span>\n' +
            '            <span style="color:#ff0000;" class="prize-count"> x '+data.prize.num+'</span>\n' +
            '        </p>\n' +
            '        <a href="Javascript:;" onclick="show(\'prize-list\')" style="cursor: pointer;">查看奖品列表</a>&nbsp;&nbsp;&nbsp;&nbsp;' +
            '<a href="Javascript:;" onclick="drawedlist(\'prize-list\')" style="cursor: pointer;color:red;">查看中奖列表</a>';
        //构建滚动列表
        var drawlist = '';
        $.each(data.users,function(key,user){
            drawlist += '<div class="clearfix lottery-list draw-users  code-'+user.code+'" code="'+user.code+'" data-id="" data-namezh="">\n' +
                '        <div class="f-l lottery-content">\n' +
                '            <em class="beauty border-01"></em>\n' +
                '            <em class="beauty border-02"></em>\n' +
                '            <em class="beauty border-03"></em>\n' +
                '            <em class="beauty border-04"></em>\n' +
                '            <div class="border bor-top"></div>\n' +
                '            <div class="border bor-bottom"></div>\n' +
                '            <div class="content-detail">\n' +
                '                \n' +
                '                <span>'+user.name+'</span>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '    </div>';
        });
        $('.prize').html(prizeHtml);
        $('#lottery-wrap').html(drawlist);
        drawUser = data.userinfo; //赋值全局变量
        drawlistMove (data.userinfo.code);
    }
    function drawlistMove(code)
    {
        var isMove = true;

        var moveDom = document.getElementById('lottery-wrap'),
            wrapDom = document.getElementById('lottery-main'),
            move_height = moveDom.offsetHeight,
            wrap_height = wrapDom.offsetHeight,
            moveTop =  moveDom.offsetTop;
        var all_size = $('#lottery-wrap .lottery-list').size();
        // 随机生成停止位置的索引
        var start_index = Math.floor(Math.random() * (all_size - 4));
        var start_top = - item_outer_height * start_index;
        var moveY = start_top;

        $('#lottery-wrap').html($('#lottery-wrap').html() + $('#lottery-wrap').html());
//      $('#lottery-wrap').html($('#lottery-wrap').html());

        var justMove = function(flag) {
            timer = nextFrame(function() {
                moveY -= speed;
                moveDom.style.top = moveY + 'px';
                if (-(moveY) >= move_height) {
                    moveY = 0;
                }
                justMove(flag);
            });
        };

        if (isMove) {
            justMove(isMove);
        } else {
            return false;
        }
        var users = $('.draw-users');
        var spped = 20;
        var inteval = setInterval(function(){
            speedUp(spped);
            spped = spped-1;
            if(spped == 1){
                clearInterval(inteval);
                steping(code);
            }
        },500);
    }
</script>