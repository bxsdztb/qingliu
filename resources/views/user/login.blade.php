<!DOCTYPE html>
<html >
<head>
<meta charset="UTF-8">
<title>登录</title>
  <meta charset="utf-8">
  <meta name="robots" content="all" />
  <meta http-equiv="imagetoolbar" content="no" />
  <meta http-equiv="pragma" Content="no-cach" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta name="format-detection" content="telephone=no" />
  <meta http-equiv="keywords" content="" />
  <meta name="author" content="" />
  <meta name="copyright" content="" />
  <meta name="description" content="" />
  <meta name="HandheldFriendly" content="true" />




  <link rel="shortcut icon" href="favicon.ico">
  <link href="/js/style/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
  <link href="/js/style/css/font-awesome.css?v=4.4.0" rel="stylesheet">
  <!-- Sweet Alert -->
  <link href="/js/style/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

  <link href="/js/style/css/animate.css" rel="stylesheet">
  <link href="/js/style/css/style.css?v=4.1.0" rel="stylesheet">
  <link rel="stylesheet" href="/js/login-css/css/style.css">
</head>

<body>
<main>
  <form class="form" action="" method="post" onblur="return CheckSubmit()">
    <div class="form__cover"></div>
    <div class="form__loader">
      <div class="spinner active">
        <svg class="spinner__circular" viewBox="25 25 50 50">
          <circle class="spinner__path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"></circle>
        </svg>
      </div>
    </div>
    <div class="form__content">
      <h1>输入你的抽奖码</h1>
      <div class="styled-input">
        <input type="text" class="styled-input__input" name="code">
        <div class="styled-input__placeholder"> <span class="styled-input__placeholder-text">抽奖码</span> </div>
        <div class="styled-input__circle"></div>
      </div>
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <button type="submit" class="styled-button"> <span class="styled-button__real-text-holder"> <span class="styled-button__real-text">Submit</span> <span class="styled-button__moving-block face"> <span class="styled-button__text-holder"> <span class="styled-button__text">Submit</span> </span> </span><span class="styled-button__moving-block back"> <span class="styled-button__text-holder"> <span class="styled-button__text">提交</span> </span> </span> </span> </button>
    </div>
  </form>
</main>

</body>
</html>
<script  src="/js/login-css/js/index.js"></script>
<script src="/js/jq.js"></script>
<script src="/js/style/js/jquery.min.js?v=2.1.4"></script>
<script src="/js/style/js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->
<script src="/js/style/js/content.js?v=1.0.0"></script>

<!-- Sweet alert -->
<script src="/js/style/js/plugins/sweetalert/sweetalert.min.js"></script>
<script>
  if("{{$message}}" != ""){
    message("{{$message}}");
  }
  function message(msg) {
    swal({
      title: "提示",
      text: msg
    });
  }
</script>