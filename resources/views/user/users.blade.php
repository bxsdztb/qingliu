<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="keywords" content="" />
    <meta name="author" content="" />
    <meta name="copyright" content="" />
    <meta name="description" content="" />
    <meta name="HandheldFriendly" content="true" />
    <style>
        li{
            list-style: none;
            border-bottom: solid 2px #e8b7b7;
            height: 50px;
        }
        li span{
            -webkit-line-clamp: 2;
            background-image:-webkit-linear-gradient(bottom, rgb(86, 83, 83),rgb(64, 60, 64));
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-family: "Arial","Microsoft YaHei","黑体","宋体",sans-serif;
            font-weight: 800;
        }
        li font{
            -webkit-line-clamp: 2;
            background-image: -webkit-linear-gradient(bottom, rgb(255, 0, 0),rgb(255, 73, 3));
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            font-family: "Arial","Microsoft YaHei","黑体","宋体",sans-serif;
            font-weight: 800;
        }
    </style>
</head>
<body>
<h3 style="position: fixed;top: 0;text-align: center;">目前共{{count($data)}}份抽奖码 已领:<span class="sended">{{$sends}}</span> 未领:<span class="unsend">{{count($data) - $sends}}</span></h3>
<ul style="margin-top: 100px;">
    @foreach($data as $key=>$user)
        @if($user->is_send == 0)
            <li style="cursor: pointer;">
                <input type="button" class="send" status="{{$user->is_send}}" userid="{{$user->id}}" style="width: 20%;background: #bbc0c3;float: left;color:#694c4c;margin-right: 10px;" value="标记">
        @else
            <li style="cursor: pointer;background: rgb(27, 249, 13);">
                <input type="button" class="send" status="{{$user->is_send}}" userid="{{$user->id}}" style="width: 20%;background: #bbc0c3;float: left;color:#694c4c;margin-right: 10px;" value="已标记">
        @endif

            <div class="list-info"  name="{{$user->name}}" userid="{{$user->id}}" style="display: inline-block;width: 70%;cursor: pointer;height: 100%">
                <span style="float: left">{{$user->name}}</span>
                <font style="float: right">{{$user->code}}</font>
            </div>
        </li>
    @endforeach
</ul>

</body>
</html>
<script src="/js/jq.js"></script>
<script>
    $(document).on('click','.send',function(){
        var status = $(this).attr('status');
        var send = parseInt($('.sended').text());
        var unsend = parseInt($('.unsend').text());
        if(status == 0){
            $(this).parent('li').css('background','#1bf90d');
            $(this).val('已标记');
            $(this).attr('status',1);
            $('.sended').text(send+1);
            $('.unsend').text(unsend-1);
        }else{
            $(this).parent('li').css('background','white');
            $(this).val('标记');
            $(this).attr('status',0);
            $('.sended').text(send-1);
            $('.unsend').text(unsend+1);
        }
        var id = $(this).attr('userid');
        $.ajax({
            url:'saveuser',
            data:{status:status,id:id},
            type:'get',
            success:function(response){

            }
        });
    });
    $(document).on('click','.list-info',function(){
        var thisDom = $(this);
        var name = $(this).attr('name');
        var status = confirm('是否删除'+name+'?');
        if(!status){
            return false;
        }
        var id = $(this).attr('userid');
        $.ajax({
            url:'removeUser',
            data:{_token:"{{csrf_token()}}",id:id},
            dataType:'json',
            type:'post',
            success:function(response){
                if(response.status == 1){
                    alert(name+'已成功删除.');
                    thisDom.remove();
                }else{
                    alert(name+'删除失败.');
                }
            }
        });
    });
</script>