<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title> - 基本表单</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="keywords" content="" />
    <meta name="author" content="" />
    <meta name="copyright" content="" />
    <meta name="description" content="" />
    <meta name="HandheldFriendly" content="true" />
    <base href="/js/style/">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css?v=4.1.0" rel="stylesheet">
    
</head>

<body class="gray-bg">
<div class="ibox-content">
    <div class="row">
        <div class="col-sm-12">
            <form role="form" method="post" action="">
                <div class="form-group">
                    <label>奖品名称</label>
                    <input type="text" name="prizename" placeholder="请输入奖品名称(尽量5字之内)" class="form-control">
                </div>
                <div class="form-group">
                    <label>奖品数量</label>
                    <input type="number" name="prizecount" placeholder="请输入奖品数量" class="form-control">
                </div>
                <div class="form-group">
                    <label>
                        <input type="radio" class="i-checks" name="prizetype" value="1"><span style=" -webkit-line-clamp: 2;background-image:-webkit-linear-gradient(bottom, rgb(245, 223, 214),rgb(255, 11, 11));-webkit-background-clip: text;-webkit-text-fill-color: transparent;margin-right: 40px;">sp奖励</span>
                    </label>
                    <label>
                        <input type="radio" class="i-checks" name="prizetype" value="2"><span style=" -webkit-line-clamp: 2;background-image:-webkit-linear-gradient(bottom, rgb(230, 199, 47),rgb(222, 3, 3));-webkit-background-clip: text;-webkit-text-fill-color: transparent;margin-right: 40px;">ssr奖励</span>
                    </label>
                    <label>
                        <input type="radio" class="i-checks" name="prizetype" value="3"><span style=" -webkit-line-clamp: 2;background-image:-webkit-linear-gradient(bottom, rgb(220, 165, 216),rgb(240, 3, 255));-webkit-background-clip: text;-webkit-text-fill-color: transparent;">sr奖励</span>
                    </label>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label>提供者</label>
                    <input type="text" name="prizepublish" placeholder="请输入提供者" class="form-control">
                </div>
                <div>
                    <button class="btn btn-sm btn-primary pull-left m-t-n-xs" type="submit"><strong>提 交</strong>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="js/jquery.min.js?v=2.1.4"></script>
<script src="js/bootstrap.min.js?v=3.3.6"></script>

<!-- 自定义js -->
<script src="js/content.js?v=1.0.0"></script>

<!-- iCheck -->
<script src="js/plugins/iCheck/icheck.min.js"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>




</body>

</html>
